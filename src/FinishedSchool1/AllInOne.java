package FinishedSchool1;

import javax.imageio.ImageIO;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.net.URL;
import java.util.Scanner;


public class AllInOne {
    public static Scanner in = new Scanner(System.in);
    static int[] masa = new int[101];

    public static void FO() throws Exception {
        F0();
        int v = in.nextInt();
        if (v == 1) {F1();}
        if (v == 2) {F2();}
        if (v == 3) {F3();}
        if (v == 4) {F4();}
        if (v == 5) {F5();}
        if (v == 6) {F6();}
        if (v == 7) {F7();}
        if (v == 8) {F8();}
        if (v == 9) {F9();}
        if (v == 10) {F10();}
        if (v == 11) {F11();}
        if (v == 12) {F12();}
        if (v == 13) {F13();}
        if (v == 14) {F14();}
        if (v == 15) {F15();}
        if (v == 16) {F16();}
        if (v == 17) {F17();}
        if (v == 18) {F18();}
        if (v == 19) {F19();}
        if (v == 20) {F20();}
        if (v == 21) {F21();}
        if (v == 22) {F22();}
        if (v == 23) {F23();}
        if (v == 24) {F24();}
        if (v == 25) {F25();}
        if (v == 26) {F26();}
        if (v == 27) {F27();}
        if (v == 28) {F28();}
        if (v == 29) {F29();}
        if (v == 30) {F30();}
    }
    public static void F0() {
        System.out.println("Input number");
        System.out.println("1 - number from 1 to k");
        System.out.println("2 - multiplication table of the first k numbers");
        System.out.println("3 - the first n primes");
        System.out.println("4 - Ulam spiral print with edge length k (k-odd)");
        System.out.println("5 - Ulam spiral print with edge length k (k-even number)");
        System.out.println("6 - array of password length l, long - k (the first letter is a vowel)");
        System.out.println("7 - array of password length l, long - k (the first letter is a consonant)");
        System.out.println("8 - sort an array of length k");
        System.out.println("9 - binary to decimal");
        System.out.println("10 - decimal to binary");
        System.out.println("11 - decimal to ternary");
        System.out.println("12 - decimal to balance ternary");
        System.out.println("13 - decimal to -ternary");
        System.out.println("14 - check line on Palindrome");
        System.out.println("15 - alphabet circle");
        System.out.println("16 - the square with length k, edge of the units");
        System.out.println("//TicTac");
        System.out.println("17 - number in the 3 * 3 matrix and check for winning (in the next turn) / correct / draw, etc.");
        System.out.println("18 - 3*3 matrix in number");
        System.out.println("19 - turn bmp file");
        System.out.println("20 - refactor gif file");
        System.out.println("21 - turn wav file");
        System.out.println("22 - all fraction n/m, a<=n,m<=b");
        System.out.println("//Graphs");
        System.out.println("23 - list of edges in adjacency matrix");
        System.out.println("24 - matrix in list of edges");
        System.out.println("25 - number of ways & the shortest path & check for acyclic & ribs, can break the path");
        System.out.println("26 - number of ways & the longest path & check for acyclic & ribs, can break the path");
        System.out.println("27 - number of included in vertex");
        System.out.println("28 - number from 1 to k in ternary");
        System.out.println("29 - number from 1 to k in balance ternary");
        System.out.println("30 - number from 1 to k in balance (-ternary)");
    }
    public static void F1() {
        System.out.println("Input k");
        int k = in.nextInt();
        for (int i = 1; i <= k; i++) {
            System.out.print(i);
            System.out.print(" ");
        }
    }
    public static void F2() {
        System.out.println("Input k");
        int k = in.nextInt();
        for (int x = 1; x <= k; x++) {
            for (int y = 1; y <= k; y++) {
                System.out.print(x * y);
                System.out.print("\t");
            }
            System.out.println(" ");
        }
    }
    public static void F3() {
        System.out.println("Input k");
        int k = in.nextInt();
        int n = 0;
        int c = 2;
        boolean a;
        while (n <= k) {
            a = true;
            for (int i = 2; i <= c / 2; i++) {
                if ((c % i) == 0) {
                    a = false;
                    break;
                }
            }
            if (a) {
                System.out.print(c);
                System.out.print(" ");
                n++;
            }
            c++;
        }
    }
    public static void F4() {
        System.out.println("Input k");
        int k = in.nextInt();
        int[][] m = new int[k + 1][k + 1];
        m = ME(m, k, k, 0);
        for (int x = 1; x <= k; x++) {
            for (int y = 1; y <= k; y++) {
                boolean a = true;
                for (int i = 2; i <= (m[x][y]) / 2; i++) {
                    if (((m[x][y]) % i) == 0) {
                        a = false;
                        break;
                    }
                }
                if (a) {
                    System.out.print(m[x][y]);
                    System.out.print("\t");
                } else {
                    System.out.print(" ");
                    System.out.print("\t");
                }
            }
            System.out.println(" ");
        }
    }
    public static void F5() {
        System.out.println("Input k");
        int k = in.nextInt();
        int[][] m = new int[k + 2][k + 2];
        m = ME(m, (k + 1), (k + 1), 0);
        for (int x = 1; x <= k; x++) {
            for (int y = 2; y <= k + 1; y++) {
                boolean b = true;
                for (int i = 2; i <= (m[x][y]) / 2; i++) {
                    if (((m[x][y]) % i) == 0) {
                        b = false;
                        break;
                    }
                }
                if (b) {
                    System.out.print(m[x][y]);
                    System.out.print("\t");
                } else {
                    System.out.print(" ");
                    System.out.print("\t");
                }
            }
            System.out.println(" ");
        }
    }
    public static void F6() {
        System.out.println("Input l");
        int l = in.nextInt();
        System.out.println("Input k");
        int k = in.nextInt();
        int a;
        for (int x = 1; x <= k; x++) {
            for (int y = 1; y <= l; y++) {
                a = (int) (Math.random() * 26 + 97);
                if (y % 2 == 1) {
                    while ((a == 97) || (a == 101) || (a == 105) || (a == 111) || (a == 117) || (a == 121)) {
                        a = (int) (Math.random() * 26 + 97);
                    }
                } else {
                    while (!((a == 97) || (a == 101) || (a == 105) || (a == 111) || (a == 117) || (a == 121))) {
                        a = (int) (Math.random() * 26 + 97);
                    }
                }
                System.out.print((char) a);
            }
            System.out.println();
        }
    }
    public static void F7() {
        System.out.println("Input l");
        int l = in.nextInt();
        System.out.println("Input k");
        int k = in.nextInt();
        int a;
        for (int x = 1; x <= k; x++) {
            for (int y = 1; y <= l; y++) {
                a = (int) (Math.random() * 26 + 97);
                if (y % 2 == 1) {
                    while (!((a == 97) || (a == 101) || (a == 105) || (a == 111) || (a == 117) || (a == 121))) {
                        a = (int) (Math.random() * 26 + 97);
                    }
                } else {
                    while ((a == 97) || (a == 101) || (a == 105) || (a == 111) || (a == 117) || (a == 121)) {
                        a = (int) (Math.random() * 26 + 97);
                    }
                }
                System.out.print((char) a);
            }
            System.out.println();
        }
    }
    public static void F8() {
        System.out.println("Input k");
        int n = in.nextInt();
        int[] m = new int[n + 1];
        System.out.println("Input array");
        for (int i = 1; i <= n; i++) {
            m[i] = in.nextInt();
        }
        for (int i = n; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (m[j] > m[j + 1]) {
                    int tmp = m[j];
                    m[j] = m[j + 1];
                    m[j + 1] = tmp;
                }
            }
        }
        for (int i = 1; i <= n; i++) {
            System.out.println(m[i]);
        }
    }
    public static void F9() {
        System.out.println("Input number");
        int n = in.nextInt();
        double a = 0;
        int i = 0;
        while (!(n == 0)) {
            a = a + ((n % 10) * (Math.pow(2, i)));
            n = n / 10;
            i++;
        }
        System.out.println(a);
    }
    public static void F10() {
        System.out.println("Input number");
        int n = in.nextInt();
        double a = 0;
        int i = 0;
        while (!(n == 0)) {
            a = a + ((n % 2) * (Math.pow(10, i)));
            n = n / 2;
            i++;
        }
        System.out.println(a);
    }
    public static void F11() {
        System.out.println("Input number");
        int n = in.nextInt();
        double a = 0;
        int i = 0;
        while (!(n == 0)) {
            a = a + ((n % 3) * (Math.pow(10, i)));
            n = n / 3;
            i++;
        }
        System.out.printf("%.0f", a);
        System.out.println();
    }
    public static void F12() {
        System.out.println("Input number");
        int a = in.nextInt();
        boolean t = false;
        if (a < 0) {
            t = true;
            a = Math.abs(a);
        }
        int[] m = new int[101];
        int i = 1;
        while (a > 0) {
            int q = a / 3;
            q = Math.abs(q);
            int r = a % 3;
            r = Math.abs(r);
            if (r <= 1) {
                m[i] = r;
                a = q;
            } else {
                r = 3 - r;
                m[i] = 0 - r;
                a = q + 1;
            }
            i++;
        }
        for (int i1 = i - 1; i1 >= 1; i1--) {
            if (t) {
                m[i1] = m[i1] * (-1);
            }
            System.out.print(m[i1]);
        }
    }
    public static void F13() {
        System.out.println("Input number");
        int x = in.nextInt();
        while (x != 0) {
            System.out.print(m(x));
            x = (x - m(x)) / (-3);
        }
    }
    public static void F14() {
        System.out.println("Input string");
        String s = in.nextLine();
        System.out.println(P(s));
    }
    public static void F15() {
        int[][] m = new int[6][6];
        m = ME(m, 5, 5, 0, 89);
        for (int x = 1; x <= 5; x++) {
            for (int y = 1; y <= 5; y++) {
                System.out.print((char) (m[x][y]));
                System.out.print("\t");
                if ((x == 5) && (y == 5)) {
                    System.out.print("Z");
                }
            }
            System.out.println(" ");
        }
    }
    public static void F16() {
        System.out.println("Input k");
        int a = in.nextInt();
        int[][] m = new int[a + 1][a + 1];
        for (int i = a; i > 1; i--) {
            m[a][i] = 1;
        }
        for (int i = a; i > 1; i--) {
            m[i][1] = 1;

        }
        for (int i = 1; i < a; i++) {
            m[1][i] = 1;
        }
        for (int i = 1; i < a; i++) {
            m[i][a] = 1;
        }
        for (int x = 1; x <= a; x++) {
            for (int y = 1; y <= a; y++) {
                System.out.print(m[x][y]);
                System.out.print("\t");
            }
            System.out.println(" ");
        }
    }
    public static void F17() {
        System.out.println("Input number");
        int n = in.nextInt();
        D(n, 1);
        int[] m = new int[10];
        m = M(n, 1, m);
        boolean o = O(m);
        boolean x = X(m);
        boolean on = ON(m);
        boolean xn = XN(m);
        int ko = KO(m);
        int kx = KX(m);
        PR(kx, ko, o, x, on, xn);
    }
    public static void F18() {
        System.out.println("Input matrix");
        double a = 0;
        for (int i = 0; i <= 8; i++) {
            a = a + ((in.nextInt()) * (Math.pow(3, i)));
        }
        System.out.println(a);
    }
    public static void F19() throws Exception {
        System.out.println("Need file \"In.bmp\"");
        RandomAccessFile FIN = new RandomAccessFile(new File("In.bmp"), "rw");
        RandomAccessFile FOUT = new RandomAccessFile(new File("Out.bmp"), "rw");
        FIN.seek(18);
        int w = FIN.readByte();
        FIN.seek(22);
        int h = FIN.readByte();
        h = Math.abs(h);
        int w1;
        if (w % 4 == 0) {
            w1 = Math.abs(3 * w);
        } else {
            w1 = Math.abs(4 * ((3 * w / 4) + 1));
        }
        FIN.seek(10);
        w = FIN.readByte();
        for (int i = 0; i < h; i++) {
            FIN.seek(i * w1 + w);
            FOUT.seek((h - i - 1) * w1 + w);
            for (int i1 = 1; i1 <= w1; i1++) {
                FOUT.write(FIN.readByte());
            }
            System.out.print(i + "/");
            System.out.println(h);
        }
        FIN.seek(0);
        FOUT.seek(0);
        for (int i = 0; i < w; i++) {
            FOUT.write(FIN.read());
        }
    }
    public static void F20() throws Exception {
        System.out.println("Need file \"In.gif\"");
        URL url = new URL("file:In.gif");
        BufferedImage bufImage = ImageIO.read(url);
        Scanner ins = new Scanner(System.in);
        System.out.println("������� �������� ������");
        String str = ins.nextLine();
        ImageIO.write(bufImage, str, new File("C:/Users/������������/IdeaProjects/NCP/OutFromGif." + str));
    }
    public static void F21() throws Exception {
        System.out.println("Need file \"In.wav\"");
        RandomAccessFile FIN = new RandomAccessFile(new File("In.wav"), "rw");
        RandomAccessFile FOUT = new RandomAccessFile(new File("Out.wav"), "rw");
        FileInputStream fin = new FileInputStream("In.wav");
        FIN.seek(32);
        int k = FIN.readByte();
        int v1 = 32;
        FIN.seek(v1);
        while ((!(FIN.readByte() == 100)) || (!(FIN.readByte() == 97)) || (!(FIN.readByte() == 116)) || (!(FIN.readByte() == 97))) {
            v1++;
            FIN.seek(v1);
        }
        fin.skip(v1 + k);
        int[] t = new int[33];
        t[1] = fin.read();
        t = F(t, 1);
        t[9] = fin.read();
        t = F(t, 9);
        t[17] = fin.read();
        t = F(t, 17);
        t[25] = fin.read();
        t = F(t, 25);
        double inf = 0;
        for (int i = 1; i <= 32; i++) {
            inf = inf + ((t[i]) * (Math.pow(2, (i - 1))));
        }
        FIN.seek(0);
        for (int i = 0; i < v1 + 8; i++) {
            FOUT.write(FIN.read());
        }
        FIN.seek(v1);
        for (int i = 1; i <= (inf / k); i++) {
            FIN.seek((v1 + k) + (i - 1) * k + 1);
            FOUT.seek((int) ((((inf / k) - i) * k) + 1));
            for (int i1 = 1; i1 <= k; i1++) {
                FOUT.write(FIN.readByte());
            }
            if (i % 100000 == 0) {
                System.out.print(i + "/");
                System.out.printf("%.0f", (inf / k));
                System.out.println();
            }
        }
        FIN.seek(0);
        FOUT.seek(0);
        for (int i = 0; i < v1 + 8; i++) {
            FOUT.write(FIN.read());
        }
        PlayOut();
    }
    public static void F22() {
        System.out.println("Input a");
        int a = in.nextInt();
        System.out.println("Input b");
        int b = in.nextInt();
        int l = (b - a + 1) * (b - a + 1) + 1;
        int[] znam = new int[l];
        int[] chis = new int[l];
        double[] ans = new double[l];
        int i3 = 1;
        znam[i3] = b;
        chis[i3] = b;
        ans[i3] = ((double) znam[i3]) / ((double) chis[i3]);
        for (int i = a; i <= b; i++) {
            for (int i1 = a; i1 <= b; i1++) {
                if (!(i1 == i)) {
                    i3++;
                    znam[i3] = i;
                    chis[i3] = i1;
                    ans[i3] = ((double) znam[i3]) / ((double) chis[i3]);
                }
            }
        }
        for (int i = i3; i > 0; i--) {
            for (int j = 1; j < i; j++) {
                if (ans[j] > ans[j + 1]) {
                    double tmp = ans[j];
                    ans[j] = ans[j + 1];
                    ans[j + 1] = tmp;
                    int tmp1 = znam[j];
                    znam[j] = znam[j + 1];
                    znam[j + 1] = tmp1;
                    tmp1 = chis[j];
                    chis[j] = chis[j + 1];
                    chis[j + 1] = tmp1;
                }
            }
        }
        for (int i = 1; i < i3; i++) {
            if (ans[i] == ans[i + 1]) {
                for (int i1 = i; i1 < i3; i1++) {
                    ans[i1] = ans[i1 + 1];
                    znam[i1] = znam[i1 + 1];
                    chis[i1] = chis[i1 + 1];
                }
                i3--;
                i--;
            }
        }
        for (int i = 1; i <= i3; i++) {
            System.out.println(znam[i] + "/" + chis[i] + " " + ans[i]);
        }
    }
    public static void F23() {
        System.out.println("Input number of vertices");
        int k = in.nextInt();
        boolean[][] m = new boolean[k + 1][k + 1];
        System.out.println("Input number of ribs");
        int k1 = in.nextInt();
        System.out.println("Input (number of ribs)*(output vertex, the input vertex)");
        for (int i = 1; i <= k1; i++) {
            int x = in.nextInt();
            int y = in.nextInt();
            m[x][y] = true;
        }
        for (int i = 1; i <= k; i++) {
            for (int i1 = 1; i1 <= k; i1++) {
                System.out.print(m[i][i1]);
                System.out.print("\t");
            }
            System.out.println("");
        }
    }
    public static void F24() {
        System.out.println("Input number of vertices");
        int k = in.nextInt();
        boolean[][] m = new boolean[k + 1][k + 1];
        System.out.println("Input matrix");
        for (int i = 1; i <= k; i++) {
            for (int i1 = 1; i1 <= k; i1++) {
                int x = in.nextInt();
                m[i][i1]=(!(x==0));
            }
        }
        for (int i = 1; i <= k; i++) {
            for (int i1 = 1; i1 <= k; i1++) {
                if (m[i][i1]) {
                    System.out.print(i);
                    System.out.print(" ");
                    System.out.print(i1);
                    System.out.println("");
                }
            }
        }
    }
    public static void F25() {
        int n, a, a1, a2, a4, w, a3 = 0;
        System.out.println("Input number of vertices");
        n = in.nextInt();
        System.out.println("Input number of ribs");
        a = in.nextInt();
        System.out.println("Input first vertex");
        a4 = in.nextInt();
        System.out.println("Input last vertex");
        w = in.nextInt();
        boolean[][] matr = new boolean[n + 1][n + 1];
        System.out.println("Input (number of ribs)*(output vertex, the input vertex)");
        while (a3 < a) {
            a1 = in.nextInt();
            a2 = in.nextInt();
            matr[a1][a2] = true;
            a3 = a3 + 1;
        }
        int[] mas = new int[n + 1];
        mas[1] = w;
        masa[0] = 41341;
        if (a4 == w) {
            System.out.print("0" + a4 + " " + w);
        } else {
            a = Sum(a4, n, w, matr, 2);
            if (a > 0) {
                Sum1(a4, n, w, matr, mas, 2);
                masa[1] = a4;
                masa[masa[0]] = w;
                int t=0;
                for(int x=0; x<=n; x++)
                {
                    for(int y=0; y<=n; y++)
                    {
                        if(matr[x][y])
                        {
                            matr[x][y]=false;
                            if (Sum(a4, n, w, matr, 2) == 0)
                            {
                                t++;
                                System.out.println("If destroy " + x + "->" + y + " then no way");
                            }
                            matr[x][y]=true;
                        }
                    }
                }
                System.out.println("Number of ways: " + a);
                System.out.println("Number of ribs, may break all path: " + t);
                System.out.println("Length shortest path: " + masa[0]);
                System.out.print("Ways: ");
                for (int i1 = 1; i1 <= masa[0]; i1++) {
                    System.out.print(masa[i1] + " ");
                }
            }
            if (a == -1) {
                System.out.println("Graph has cyclic");
            }
            if ((a == 0) || (a < -1)) {
                System.out.print("No way");
            }
        }
    }
    public static void F26() {
        int n, a, a1, a2, a4, w, a3 = 0;
        System.out.println("Input number of vertices");
        n = in.nextInt();
        System.out.println("Input number of ribs");
        a = in.nextInt();
        System.out.println("Input first vertex");
        a4 = in.nextInt();
        System.out.println("Input last vertex");
        w = in.nextInt();
        boolean[][] matr;
        matr = new boolean[n + 1][n + 1];
        System.out.println("Input (number of ribs)*(output vertex, the input vertex)");
        while (a3 < a) {
            a1 = in.nextInt();
            a2 = in.nextInt();
            matr[a1][a2] = true;
            a3 = a3 + 1;
        }
        int[] mas = new int[n + 1];
        mas[1] = w;
        masa[0] = 0;
        if (a4 == w) {
            System.out.print("0" + a4 + " " + w);
        } else {
            a = Sum(a4, n, w, matr, 2);
            if (a > 0) {
                Sum2(a4, n, w, matr, mas, 2);
                masa[1] = a4;
                masa[masa[0]] = w;
                int t=0;
                for(int x=0; x<=n; x++)
                {
                    for(int y=0; y<=n; y++)
                    {
                        if(matr[x][y])
                        {
                            matr[x][y]=false;
                            if (Sum(a4, n, w, matr, 2) == 0)
                            {
                                t++;
                                System.out.println("If destroy " + x + "->" + y + " then no way");
                            }
                            matr[x][y]=true;
                        }
                    }
                }
                System.out.println("Number of ways: " + a);
                System.out.println("Number of ribs, may break all path: " + t);
                System.out.println("Length longest path: " + masa[0]);
                System.out.print("Ways: ");
                for (int i1 = 1; i1 <= masa[0]; i1++) {
                    System.out.print(masa[i1] + " ");
                }
            }
            if (a == -1) {
                System.out.println("Graph has cyclic");
            }
            if ((a == 0) || (a < -1)) {
                System.out.print("No way");
            }
        }
    }
    public static void F27() {
        int n, a, a3 = 0, a1, a2;
        System.out.println("Input number of vertices");
        n = in.nextInt();
        System.out.println("Input number of ribs");
        a = in.nextInt();
        boolean[][] matr;
        matr = new boolean[n + 1][n + 1];
        System.out.println("Input (number of ribs)*(output vertex, the input vertex)");
        while (a3 < a) {
            a1 = in.nextInt();
            a2 = in.nextInt();
            matr[a1][a2] = true;
            a3 = a3 + 1;
        }
        for (a1 = 1; a1 <= n; a1++) {
            a3 = 0;
            for (a2 = 1; a2 <= n; a2++) {
                if (matr[a2][a1]) {
                    a3++;
                }
            }
            System.out.println("Number in. edges for " + a1 + "-vertex: " + a3);
        }
    }
    public static void F28() {
        System.out.println("Input k");
        int k = in.nextInt();
        for(int i1=1; i1<=k; i1++) {
            int n = i1;
            double a = 0;
            int i = 0;
            while (!(n == 0)) {
                a = a + ((n % 3) * (Math.pow(10, i)));
                n = n / 3;
                i++;
            }
            System.out.printf("%.0f", a);
            System.out.println();
        }
    }
    public static void F29() {
        System.out.println("Input k");
        int k = in.nextInt();
        for(int i2=1; i2<=k; i2++) {
            int a = i2;
            boolean t = false;
            if (a < 0) {
                t = true;
                a = Math.abs(a);
            }
            int[] m = new int[101];
            int i = 1;
            while (a > 0) {
                int q = a / 3;
                q = Math.abs(q);
                int r = a % 3;
                r = Math.abs(r);
                if (r <= 1) {
                    m[i] = r;
                    a = q;
                } else {
                    r = 3 - r;
                    m[i] = 0 - r;
                    a = q + 1;
                }
                i++;
            }
            for (int i1 = i - 1; i1 >= 1; i1--) {
                if (t) {
                    m[i1] = m[i1] * (-1);
                }
                System.out.print(m[i1]);
            }
            System.out.println();
        }
    }
    public static void F30() {
        System.out.println("Input k");
        int k = in.nextInt();
        for(int i2=1; i2<=k; i2++) {
            int a = i2;
            boolean t = false;
            if (a < 0) {
                t = true;
                a = Math.abs(a);
            }
            int[] m = new int[101];
            int i = 1;
            while (a > 0) {
                int q = a / 3;
                q = Math.abs(q);
                int r = a % 3;
                r = Math.abs(r);
                if (r <= 1) {
                    m[i] = r;
                    a = q;
                } else {
                    r = 3 - r;
                    m[i] = 0 - r;
                    a = q + 1;
                }
                i++;
            }
            for (int i1 = i - 1; i1 >= 1; i1--) {
                if (t) {
                    m[i1] = m[i1] * (-1);
                }
                System.out.print((-1)*(m[i1]));
            }
            System.out.println();
        }
    }


    public static int[][] ME(int[][] m, int k, int c, int iter) {
        iter++;
        if (iter == (c / 2) + 1) {
            m[iter][iter] = 4;
            return m;
        }
        int a = k * k;
        for (int i = c - iter + 1; i > iter; i--) {
            m[c - iter + 1][i] = a;
            a--;
        }
        for (int i = c - iter + 1; i > iter; i--) {
            m[i][iter] = a;
            a--;
        }
        for (int i = iter; i < c - iter + 1; i++) {
            m[iter][i] = a;
            a--;
        }
        for (int i = iter; i < c - iter + 1; i++) {
            m[i][c - iter + 1] = a;
            a--;
        }
        ME(m, (k - 2), c, iter);
        return m;
    }
    public static int[] F(int[] t, int q) {
        int n = t[q];
        while (!(n == 0)) {
            t[q] = (n % 2);
            n = n / 2;
            q++;
        }
        return t;
    }
    public static void PlayOut() throws Exception {
        URL urlOut = new URL("file:Out.wav");
        AudioClip Out = Applet.newAudioClip(urlOut);
        Out.play();
        System.out.println("Press any key to exit.");
        System.in.read();
        Out.stop();
    }
    public static int[][] ME(int[][] m, int k, int c, int iter, int a) {
        iter++;
        if (iter == (c / 2) + 1) {
            m[iter][iter] = a;
            return m;
        }
        for (int i = c - iter + 1; i > iter; i--) {
            m[c - iter + 1][i] = a;
            a--;
        }
        for (int i = c - iter + 1; i > iter; i--) {
            m[i][iter] = a;
            a--;
        }
        for (int i = iter; i < c - iter + 1; i++) {
            m[iter][i] = a;
            a--;
        }
        for (int i = iter; i < c - iter + 1; i++) {
            m[i][c - iter + 1] = a;
            a--;
        }
        ME(m, (k - 2), c, iter, a);
        return m;
    }
    public static int m(int x) {
        int i = 0;
        while (i < 3) {
            int y = (x - i) % (-3);
            if (y == 0) {
                return i;
            } else {
                i++;
            }
        }
        return 0;
    }
    public static Boolean P(String s) {
        return s.equals((new StringBuilder(s)).reverse().toString());
    }
    public static int D(int n, int iter) {
        if (iter < 10) {
            int a = n % 3;
            if (a == 0) {
                System.out.print("O");
            }
            if (a == 1) {
                System.out.print("X");
            }
            if (a == 2) {
                System.out.print("�");
            }
            if (iter % 3 == 0) {
                System.out.println("");
            }
            iter++;
            D((n / 3), iter);
        }
        return 0;
    }
    public static int[] M(int n, int iter, int[] m) {
        if (iter < 10) {
            int a = n % 3;
            if (a == 0) {
                m[iter] = 0;
            }
            if (a == 1) {
                m[iter] = 1;
            }
            if (a == 2) {
                m[iter] = 2;
            }
            iter++;
            M((n / 3), iter, m);
        }
        return m;
    }
    public static boolean O(int[] m) {
        if (m[1] == 0) {
            if ((m[1] == m[2]) && (m[1] == m[3])) {
                return true;
            }
            if ((m[1] == m[4]) && (m[1] == m[7])) {
                return true;
            }
            if ((m[1] == m[5]) && (m[1] == m[9])) {
                return true;
            }
        }
        if (m[5] == 0) {
            if ((m[5] == m[2]) && (m[5] == m[8])) {
                return true;
            }
            if ((m[5] == m[4]) && (m[5] == m[6])) {
                return true;
            }
            if ((m[5] == m[3]) && (m[5] == m[7])) {
                return true;
            }

        }
        if (m[9] == 0) {
            if ((m[9] == m[7]) && (m[9] == m[8])) {
                return true;
            }
            if ((m[9] == m[3]) && (m[9] == m[6])) {
                return true;
            }
        }
        return false;
    }
    public static boolean X(int[] m) {
        if (m[1] == 1) {
            if ((m[1] == m[2]) && (m[1] == m[3])) {
                return true;
            }
            if ((m[1] == m[4]) && (m[1] == m[7])) {
                return true;
            }
            if ((m[1] == m[5]) && (m[1] == m[9])) {
                return true;
            }
        }
        if (m[5] == 1) {
            if ((m[5] == m[2]) && (m[5] == m[8])) {
                return true;
            }
            if ((m[5] == m[4]) && (m[5] == m[6])) {
                return true;
            }
            if ((m[5] == m[3]) && (m[5] == m[7])) {
                return true;
            }

        }
        if (m[9] == 1) {
            if ((m[9] == m[7]) && (m[9] == m[8])) {
                return true;
            }
            if ((m[9] == m[3]) && (m[9] == m[6])) {
                return true;
            }
        }
        return false;
    }
    public static int KO(int[] m) {
        int ko = 0;
        for (int i = 1; i <= 9; i++) {
            if (m[i] == 0) {
                ko = ko + 1;
            }
        }
        return ko;
    }
    public static int KX(int[] m) {
        int kx = 0;
        for (int i = 1; i <= 9; i++) {
            if (m[i] == 1) {
                kx = kx + 1;
            }
        }
        return kx;
    }
    public static boolean ON(int[] m) {
        if (m[1] == 0) {
            if ((m[1] == m[2]) && (m[3] == 2)) {
                return true;
            }
            if ((m[1] == m[3]) && (m[2] == 2)) {
                return true;
            }
            if ((m[1] == m[4]) && (m[7] == 2)) {
                return true;
            }
            if ((m[1] == m[7]) && (m[4] == 2)) {
                return true;
            }
            if ((m[1] == m[9]) && (m[5] == 2)) {
                return true;
            }
            if ((m[1] == m[5]) && (m[9] == 2)) {
                return true;
            }
        }
        if (m[5] == 0) {
            if ((m[5] == m[9]) && (m[1] == 2)) {
                return true;
            }
            if ((m[5] == m[8]) && (m[2] == 2)) {
                return true;
            }
            if ((m[5] == m[2]) && (m[8] == 2)) {
                return true;
            }
            if ((m[5] == m[6]) && (m[4] == 2)) {
                return true;
            }
            if ((m[5] == m[4]) && (m[6] == 2)) {
                return true;
            }
            if ((m[5] == m[7]) && (m[3] == 2)) {
                return true;
            }
            if ((m[5] == m[3]) && (m[7] == 2)) {
                return true;
            }
        }
        if (m[9] == 0) {
            if ((m[9] == m[6]) && (m[3] == 2)) {
                return true;
            }
            if ((m[9] == m[3]) && (m[6] == 2)) {
                return true;
            }
            if ((m[9] == m[8]) && (m[7] == 2)) {
                return true;
            }
            if ((m[9] == m[7]) && (m[8] == 2)) {
                return true;
            }
        }
        if (m[1] == 2) {
            if ((m[2] == 0) && (m[2] == m[3])) {
                return true;
            }
            if ((m[4] == 0) && (m[4] == m[7])) {
                return true;
            }
        }
        if (m[5] == 2) {
            if ((m[2] == 0) && (m[2] == m[8])) {
                return true;
            }
            if ((m[3] == 0) && (m[3] == m[7])) {
                return true;
            }
            if ((m[4] == 0) && (m[4] == m[6])) {
                return true;
            }
        }
        if (m[9] == 2) {
            if ((m[3] == 0) && (m[3] == m[6])) {
                return true;
            }
            if ((m[7] == 0) && (m[7] == m[8])) {
                return true;
            }
        }
        return false;
    }
    public static boolean XN(int[] m) {
        if (m[1] == 1) {
            if ((m[1] == m[2]) && (m[3] == 2)) {
                return true;
            }
            if ((m[1] == m[3]) && (m[2] == 2)) {
                return true;
            }
            if ((m[1] == m[4]) && (m[7] == 2)) {
                return true;
            }
            if ((m[1] == m[7]) && (m[4] == 2)) {
                return true;
            }
            if ((m[1] == m[9]) && (m[5] == 2)) {
                return true;
            }
            if ((m[1] == m[5]) && (m[9] == 2)) {
                return true;
            }
        }
        if (m[5] == 1) {
            if ((m[5] == m[9]) && (m[1] == 2)) {
                return true;
            }
            if ((m[5] == m[8]) && (m[2] == 2)) {
                return true;
            }
            if ((m[5] == m[2]) && (m[8] == 2)) {
                return true;
            }
            if ((m[5] == m[6]) && (m[4] == 2)) {
                return true;
            }
            if ((m[5] == m[4]) && (m[6] == 2)) {
                return true;
            }
            if ((m[5] == m[7]) && (m[3] == 2)) {
                return true;
            }
            if ((m[5] == m[3]) && (m[7] == 2)) {
                return true;
            }
        }
        if (m[9] == 1) {
            if ((m[9] == m[6]) && (m[3] == 2)) {
                return true;
            }
            if ((m[9] == m[3]) && (m[6] == 2)) {
                return true;
            }
            if ((m[9] == m[8]) && (m[7] == 2)) {
                return true;
            }
            if ((m[9] == m[7]) && (m[8] == 2)) {
                return true;
            }
        }
        if (m[1] == 2) {
            if ((m[2] == 1) && (m[2] == m[3])) {
                return true;
            }
            if ((m[4] == 1) && (m[4] == m[7])) {
                return true;
            }
        }
        if (m[5] == 2) {
            if ((m[2] == 1) && (m[2] == m[8])) {
                return true;
            }
            if ((m[3] == 1) && (m[3] == m[7])) {
                return true;
            }
            if ((m[4] == 1) && (m[4] == m[6])) {
                return true;
            }
        }
        if (m[9] == 2) {
            if ((m[3] == 1) && (m[3] == m[6])) {
                return true;
            }
            if ((m[7] == 1) && (m[7] == m[8])) {
                return true;
            }
        }
        return false;
    }
    public static int PR(int kx, int ko, boolean o, boolean x, boolean on, boolean xn) {
        if ((kx == ko) || (kx == (ko + 1))) {
            if (o == x) {
                if (o) {
                    System.out.println("Wrong");
                } else {
                    if (kx + ko == 9) {
                        System.out.println("FinishedDraw");
                    } else {
                        if (kx == ko) {
                            if (xn) {
                                System.out.println("InNextStepWin.X");
                            } else {
                                System.out.println("NotFinished");
                            }
                        }
                        if (kx == (ko + 1)) {
                            if (on) {
                                System.out.println("InNextStepWin.O");
                            } else {
                                System.out.println("NotFinished");
                            }
                        }
                    }
                }
            } else {
                if (o) {
                    System.out.println("Win.O");
                } else {
                    System.out.println("Win.X");
                }
            }
        } else {
            System.out.println("Wrong");
        }
        return 0;
    }
    public static int Sum(int b, int c, int b1, boolean[][] arr, int iter) {
        if (iter > c) {
            return -1;
        }
        int r = 0;
        for (int i = 1; i <= c; i++) {
            if (arr[b][i]) {
                if (i == b1) {
                    r = r + 1;
                } else {
                    r = r + Sum(i, c, b1, arr, iter + 1);
                }
            }
        }
        return r;
    }
    public static void Sum1(int b, int c, int b1, boolean[][] arr, int[] mas, int iter) {
        mas[0] = iter;
        for (int i = 1; i <= c; i++) {
            if (arr[b][i]) {
                if (i == b1) {
                    if (mas[0] < masa[0]) {
                        for (int i1 = 0; i1 <= mas[0]; i1++) {
                            masa[i1] = mas[i1];
                        }
                    }
                } else {
                    mas[iter] = i;
                    Sum1(i, c, b1, arr, mas, iter + 1);
                }
            }
        }
    }
    public static void Sum2(int b, int c, int b1, boolean[][] arr, int[] mas, int iter) {
        mas[0] = iter;
        if (iter > c) {
            return;
        }
        for (int i = 1; i <= c; i++) {
            if (arr[b][i]) {
                if (i == b1) {
                    if (mas[0] > masa[0]) {
                        for (int i1 = 0; i1 <= mas[0]; i1++) {
                            masa[i1] = mas[i1];
                        }
                    }
                } else {
                    mas[iter] = i;
                    Sum2(i, c, b1, arr, mas, iter + 1);
                }
            }
        }
    }

    public static void main(String args[]) throws Exception {
        FO();
    }
}