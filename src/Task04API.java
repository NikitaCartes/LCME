//All tasks with Wolfram|Alpha API

import com.wolfram.alpha.WAEngine;
import com.wolfram.alpha.WAException;
import com.wolfram.alpha.WAPlainText;
import com.wolfram.alpha.WAPod;
import com.wolfram.alpha.WAQuery;
import com.wolfram.alpha.WAQueryResult;
import com.wolfram.alpha.WASubpod;

import java.util.Scanner;

public class Task04API {

    private static String appid = "W62TWQ-E6J4H9JT29";

    public static void main(String[] args) {

        System.out.println("Input something" + (char) 10 + "For example:" + (char) 10 + "1) gcd(1255, 3621)" + (char) 10 + "2) phi(346)" + (char) 10 + "3) factor(3412)" + (char) 10 + "4) sqrt(11234)");
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        if (args.length > 0)
            input = args[0];
        WAEngine engine = new WAEngine();
        engine.setAppID(appid);
        engine.addFormat("plaintext");
        WAQuery query = engine.createQuery();
        query.setInput(input);
        try {
            WAQueryResult queryResult = engine.performQuery(query);
            if (queryResult.isError()) {
                System.out.println("Query error");
                System.out.println("  error code: " + queryResult.getErrorCode());
                System.out.println("  error message: " + queryResult.getErrorMessage());
            } else if (!queryResult.isSuccess()) {
                System.out.println("Query was not understood; no results available.");
            } else {
                System.out.println("Successful query. Pods follow:\n");
                int i = 1;
                for (WAPod pod : queryResult.getPods()) {
                    if ((!pod.isError()) && (i <= 2)) {
                        System.out.println(pod.getTitle());
                        System.out.println("------------");
                        i++;
                        for (WASubpod subpod : pod.getSubpods()) {
                            for (Object element : subpod.getContents()) {
                                if (element instanceof WAPlainText) {
                                    System.out.println(((WAPlainText) element).getText());
                                    System.out.println("");
                                }
                            }
                        }
                        System.out.println("");
                    }
                }
            }
        } catch (WAException e) {
            e.printStackTrace();
        }
    }
}
