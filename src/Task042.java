//Euclidean algorithm with division

import java.util.Scanner;

public class Task042 {

    public static void main(String args[]) throws Exception {
        Scanner in = new Scanner(System.in);
        long a = in.nextLong();
        long b = in.nextLong();
        long r;
        if (b >= a) {
            r = a;
            a = b;
            b = r;
        }
        while (true) {
            if (a % b == 0) {
                System.out.println(b);
                break;
            }
            r = b;
            b = a % b;
            a = r;
        }
    }
}