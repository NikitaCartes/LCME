//1.2 X1 + X2 + � + Xn = s, 0 ? Xi ? k

import java.util.Scanner;


public class Task012 {
    public static double f11(int n, int k, int m, int[] a, int kt, int b) {
        double t = 0;
        if (k == 0) {
            return 0;
        }
        k = k - 1;
        for (int i = 0; i <= m; i++) {
            a[kt - k] = i;
            int te = f12(a, kt - k);
            if (te == n) {
                if (b == 1) {
                    f13(a, kt - k, kt);
                    System.out.println(" ");
                }
                return (t + 1);
            }
            if (te > n) {
                return t;
            }
            if (te < n) {
                t = f11(n, k, m, a, kt, b) + t;
            }
        }
        return t;
    }

    public static int f12(int[] a, int i1) {
        int t = 0;
        for (int i = 1; i <= i1; i++) {
            t = t + a[i];
        }
        return t;
    }

    public static void f13(int[] a, int i1, int i2) {
        for (int i = 1; i <= i1; i++) {
            System.out.print(a[i] + " ");
        }
        for (int i = i1 + 1; i <= i2; i++) {
            System.out.print("0 ");
        }
    }

    public static void main(String args[]) throws Exception {
        Scanner in = new Scanner(System.in);
        System.out.println("Input s, n, k or exit");
        String str = in.nextLine();
        if (str.equals("exit")) {
            return;
        }
        String it[] = str.split(" ");
        int n = Integer.parseInt(it[0]);
        int k = Integer.parseInt(it[1]);
        int m = Integer.parseInt(it[2]);
        int[] a = new int[m + 1];
        System.out.println("Vivodit' chisla? 0 - no, 1 - yes");
        int b = in.nextInt();
        System.out.println(f11(n, k, m, a, k, b));
        main(new String[0]);
    }
}
