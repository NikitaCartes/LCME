//Technical file


class Tree {
    public Tree left;
    public Tree right;
    public char id;

    public Tree(Tree x, Tree y) {
        left = x;
        right = y;
    }

    public Tree(char i) {
        left = right = null;
        id = i;
    }
}
