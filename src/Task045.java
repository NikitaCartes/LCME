//phi(x) for not long, without factor(x)(alg. from me)

import java.util.Scanner;

public class Task045 {
    public static double X(double a, double b) throws Exception {
        double r;
        while (true) {
            if (a % b == 0) {
                return b;
            }
            r = b;
            b = a % b;
            a = r;
        }
    }

    public static boolean[] Y(boolean[] m, int k, int n1) throws Exception {
        int n;
        int i = 1;
        while (true) {
            n = i * k;
            if (n >= n1) {
                return m;
            } else {
                m[n] = true;
                i++;
            }
        }
    }

    public static void main(String args[]) throws Exception {
        Scanner in = new Scanner(System.in);
        int n1 = in.nextInt();
        int c = 1;
        int a = 1;
        boolean[] m = new boolean[n1 + 1];
        if (n1 % 2 == 0) {
            a = 2;
        } else {
            c++;
        }
        for (int k = 3; k <= n1; k = k + a) {
            if (!(m[k])) {
                if (X(k, n1) == 1) {
                    c++;
                } else {
                    m = Y(m, k, n1);
                }
            }
        }
        System.out.println(c);
    }
}