//Task031 + prefix notation for characters occurring

import javax.tools.*;
import java.io.*;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Task032 {
    static class MyClassLoader extends ClassLoader {

        public Class getClassFromFile(File f) {
            byte[] raw = new byte[(int) f.length()];
            InputStream in = null;
            try {
                in = new FileInputStream(f);
                in.read(raw);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (in != null)
                    in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return defineClass(null, raw, 0, raw.length);
        }
    }

    public static void main(String args[]) throws Exception {
        FileWriter file = new FileWriter(new File("MyClass.java"));
        int[] m = Task031.main(new String[0]);
        file.write("import java.io.File;\n" +
                "import java.io.FileWriter;\n" +
                "import java.util.Arrays;\n" +
                "\n" +
                "public class MyClass {\n" +
                "    public static String[] l;\n" +
                "\n" +
                "    public static void stat(Tree t, String s) {\n" +
                "        if (t.left == null) {\n" +
                "            l[(int) t.id] = s;\n" +
                "        } else {\n" +
                "            stat(t.left, s + \"0\");\n" +
                "            stat(t.right, s + \"1\");\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    public static void main(String args[]) throws Exception {\n" +
                "        l = new String[300];\n");
        file.write("        Tree t = ");
        int k1 = 0;
        for (int i = 0; i <= 255; i++) {
            if (!(m[i] == 0)) {
                k1 = i;
            }
        }
        for (int i = 0; i <= k1 - 1; i++) {
            if (!(m[i] == 0)) {
                file.write("new Tree(new Tree((char) " + i + "), ");
            }
        }
        file.write("new Tree((char) " + k1 + ")");
        for (int i = 0; i <= k1 - 1; i++) {
            if (!(m[i] == 0)) {
                file.write(")");
            }
        }
        file.write(";\n");
        file.write("        FileWriter file = new FileWriter(new File(\"Answer.txt\"), true);\n" +
                "        Arrays.fill(l, \"\");\n" +
                "        stat(t, \"\");\n" +
                "        for (int i = 0; i < l.length; i++) {\n" +
                "            if (l[i] != \"\") {\n" +
                "                file.write(((char) i) + \" - \" + l[i] + (char) 10);\n" +
                "            }\n" +
                "        }\n" +
                "        file.close();\n" +
                "    }\n" +
                "}");
        file.flush();
        file.close();
        try {
            String fileName = "MyClass.java";
            JavaCompiler javac = ToolProvider.getSystemJavaCompiler();
            StandardJavaFileManager fileManager = javac.getStandardFileManager(null, null, null);
            DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
            Iterable<? extends JavaFileObject> compilationUnits1 = fileManager.getJavaFileObjectsFromFiles(Arrays.asList(new File[]{new File(fileName)}));
            javac.getTask(null, fileManager, diagnostics, null, null, compilationUnits1).call();
            for (Diagnostic diagnostic : diagnostics.getDiagnostics())
                System.out.println(diagnostic);
            fileManager.close();

            MyClassLoader loader1 = new MyClassLoader();
            Class my1 = loader1.getClassFromFile(new File("MyClass.class"));
            Method m1 = my1.getMethod("main", new Class[]{String[].class});
            Object o1 = my1.newInstance();
            m1.invoke(o1, new Object[]{new String[0]});
        } catch (Exception e) {
            e.printStackTrace();
        }
        File f = new File("MyClass.java");
        f.delete();
        File f1 = new File("MyClass.class");
        f1.delete();
    }
}
