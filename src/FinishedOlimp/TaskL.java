package FinishedOlimp;

import java.io.IOException;
import java.util.Scanner;


public class TaskL {
    public static void main(String[] s) throws IOException {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m0[] = new int[2*n];
        int m1[] = new int[2*n];
        for (int i = 1; i <= n; i++)
        {
            int a=in.nextInt();
            if (a == 0)
            {
                m0[i] = in.nextInt();
            }
            else
            {
                m1[i] = in.nextInt();
            }
        }
        for(int i=n; i > 1 ; i--)
        {
            for(int j = 1 ; j <= i ; j++)
            {
                if( m0[j] < m0[j+1] )
                {
                    int tmp = m0[j];
                    m0[j] = m0[j+1];
                    m0[j+1] = tmp;
                }
            }
        }
        for(int i=n; i > 1 ; i--)
        {
            for(int j = 1 ; j <= i ; j++)
            {
                if( m1[j] < m1[j+1] )
                {
                    int tmp = m1[j];
                    m1[j] = m1[j+1];
                    m1[j+1] = tmp;
                }
            }
        }
        for (int j = 1; j <n; j++)
        {
            if (m1[j] < m1[j + 1])
            {
                int tmp = m1[j];
                m1[j] = m1[j + 1];
                m1[j + 1] = tmp;
            }
        }
        int i0=1;
        for(int i=1; i<=n; i++)
        {
            if (m0[i]>0)
            {
                m0[i0] = m0[i];
                i0++;
            }
        }
        for(int i=1; i<=n; i++)
        {
            if (m1[i]>0)
            {
                m0[i0] = m1[i];
                i0++;
            }
        }
        i0=0;
        for(int i=1; i<n; i++)
        {
            if(Math.abs(m0[i]-m0[i+1])>i0)
            {
                i0=Math.abs((m0[i]-m0[i+1]));
            }
        }
        System.out.println(i0);
    }
}
