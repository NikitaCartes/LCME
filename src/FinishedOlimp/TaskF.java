package FinishedOlimp;

import java.util.*;
import java.io.*;

public class TaskF {
    public static int M(int n)
    {
        int i1=0;
        for(int i=1; i<=(Math.sqrt(n)); i++)
        {
            if(n%i==0)
            {
                i1=i1+1;
            }
        }
        if (((Math.sqrt(n))*(Math.sqrt(n)))==n)
        {
            return ((2*i1)-1);
        }
        return (2*i1);
    }
    public static void main(String[] s) throws IOException {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int max=0;
        int max_i=0;
        int max_t;
        for(int i=1; i<=n; i++)
        {
            max_t=M(i);
            if(max_t>=max)
            {
                max=max_t;
                max_i=i;
            }
        }
        System.out.println(max_i);
        System.out.println(max);
    }
}