package FinishedOlimp;

import java.util.*;
import java.io.*;

public class TaskB {
    public static void main(String[] s) throws IOException {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        boolean t = false;
        if (a < 0) {
            t = true;
            a = Math.abs(a);
        }
        int[] m = new int[101];
        int i = 1;
        while (a > 0) {
            int q = a / 3;
            q = Math.abs(q);
            int r = a % 3;
            r = Math.abs(r);
            if (r <= 1) {
                m[i] = r;
                a = q;
            } else {
                r = 3 - r;
                m[i] = 0 - r;
                a = q + 1;
            }
            i++;
        }
        for (int i1 = i - 1; i1 >= 1; i1--) {
            if (t) {
                m[i1] = m[i1] * (-1);
            }
            if(m[i1]==(-1))
            {
                System.out.print("$");
            }
            else {
                System.out.print(m[i1]);
            }
        }
    }
}
