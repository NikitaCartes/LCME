package FinishedOlimp;

import java.util.*;
import java.io.*;

public class TaskC {
    public static int[] N(int n, int k, int[] m, int i)
    {
        return m;
    }

    public static void main(String[] s) throws IOException {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int[] m = new int[k + 1];
        boolean t = true;
        m[1] = in.nextInt();
        for (int i = 2; i <= k; i++) {
            m[i] = in.nextInt();
            if (!(m[i]==m[i-1]+1))
            {
                t = false;
            }
        }
        if (((t)&&(m[k]==n)) || (k == n)) {
            System.out.print("-1");
            return;
        }
        t=false;
        for (int i = 2; i<=k; i++)
        {
            if(m[i]==(n-(k-i)))
            {
                m[i-1]=m[i-1]+1;
                for(int i1=i; i1<=k; i1++)
                {
                    m[i1]=m[i1-1]+1;
                }
                t=true;
                break;
            }
        }
        if(t)
        {
            for (int i = 1; i <= k; i++)
            {
                System.out.print(m[i]);
                System.out.print(" ");
            }
        }
        else
        {
            for (int i = 1; i < k; i++)
            {
                System.out.print(m[i]);
                System.out.print(" ");
            }
            System.out.print(m[k]+1);
        }
    }
}