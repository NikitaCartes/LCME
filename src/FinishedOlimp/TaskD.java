package FinishedOlimp;

import java.util.*;
import java.io.*;

public class TaskD {
    public static int iter;
    public static int M(int[] m, int a, int f)
    {
        if(a>2) {
            for (int i = 2; i < a; i++) {
                int m2[] = new int[a];
                for (int i1 = 1; i1 < i; i1++) {
                    m2[i1] = m[i1];
                }
                for (int i1 = i; i1 < a; i1++) {
                    m2[i1] = m[i1 + 1];
                }
                M(m2,a-1,(f+(m[i]*(m[i-1]+m[i+1]))));
            }
        }
        else
        {
            if(iter>=f)
            {
                iter=f;
            }
            return 0;
        }
        return 0;
    }
    public static void main(String[] s) throws IOException {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        if (a>2) {
            int m[] = new int[a + 1];
            for (int i = 1; i <= a; i++) {
                m[i] = in.nextInt();
            }
            iter=0;
            for (int i = 2; i < a; i++) {
                iter=iter+(m[i]*(m[i-1]+m[i+1]));
            }
            M(m,a,0);
            System.out.print(iter);
        }
        else
        {
            System.out.print("0");
        }
    }
}