//Task031 + prefix notation for characters occurring

import java.io.File;
import java.io.FileWriter;


public class Task032WithoutTree {

    public static void main(String args[]) throws Exception {
        int[] m = Task031.main(new String[0]);
        String[] tr = new String[256];
        FileWriter file = new FileWriter(new File("Answer.txt"), true);
        int k1 = 0;
        for (int i = 0; i <= 255; i++) {
            if (!(m[i] == 0)) {
                k1 = i;
                break;
            }
        }
        tr[k1] = "0";
        for (int i = k1 + 1; i <= 255; i++) {
            if (!(m[i] == 0)) {
                tr[i] = "1" + tr[k1];
                k1 = i;
            }
        }
        String k = "";
        for (int i = 0; i <= 255; i++) {
            if (!(m[i] == 0)) {
                k1 = i;
                k = k + "1";
            }
        }
        tr[k1] = k.substring(0, k.length() - 1);
        for (int i = 0; i <= 255; i++) {
            if (!(m[i] == 0)) {
                file.write(((char) i) + " - " + tr[i] + (char) 10);
            }
        }
        file.close();
    }
}